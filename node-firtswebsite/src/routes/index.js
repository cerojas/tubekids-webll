const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.render('index', { title: 'TubeKids' });
});

router.get('/contact', (req, res) => {
  res.render('contact', { title: 'Register' });
});
router.get('/login', (req, res) => {
  res.render('login', { title: 'Login' });
});

module.exports = router;